export class CreateUserDto {
  fullName: string;
  email: string;
  password: string;
  gender: string;
  image: string;
  roles: string;
}
